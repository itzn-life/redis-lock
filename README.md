# redis-lock

#### 介绍
redis-lock是基于redis实现的分布式锁，三种使用方式和SpEL的支持，一个注解搞定，让分布式锁不再繁琐

#### 软件架构
1.  RedisLock 实现Redis分布式锁的具体实现，实现了java.util.concurrent.locks.Lock，与其使用方式无差别
2.  LockFactory 分布式锁工厂类，用于获取RedisLock，并实现了缓存复用，定时清理，以及按默认时间、自定义时间获取锁
    ```
    // 按全局超时时间获取分布式锁
    RedisLock redisLock = this.lockFactory.get(key);
    
    // 按自定义超时时间获取分布式锁
    RedisLock redisLock = this.lockFactory.get(key, 5, TimeUnit.MINUTES);
    ```
3.  MethodLock 基于注解实现的分布式方法锁，可以自定义key，时间单位
4.  LockAspect 用于支持MethodLock实现AOP功能，支持SpEL表达式
5.  LockAutoConfiguration 用于Redis分布式锁组件的自动配置


#### 安装教程

1.  导入依赖
```
<dependency>
    <groupId>life.itzn</groupId>
    <artifactId>redis-lock</artifactId>
    <version>1.0.0</version>
</dependency>
```
2.  配置Redis
```
spring:
  redis:
    host: 127.0.0.1
    port: 6379
```
3.  自定义配置
```
lock:
    prefix: lock    全局锁名前缀，默认为：lock
    time: 60        全局超时时间，默认为：60
    unit: SECONDS   全局超时单位，默认为：秒
```

#### 使用说明

1.  代码方式-手动释放锁
```
@Autowired
private LockFactory lockFactory;
 
@GetMapping("/{key}")
public String lock1(@PathVariable String key) {
    RedisLock redisLock = this.lockFactory.get(key);
    try {
        if (!redisLock.tryLock(5, TimeUnit.SECONDS)) {
            return "按自定义锁时间获取锁失败：" + key;
        }
    } finally {
        redisLock.unlock();
    }
    return "获取锁成功：" + key;
}
```
2.  代码方式-自动释放锁
```
@Autowired
private LockFactory lockFactory;
 
@GetMapping("/{key}")
public String lock2(@PathVariable String key) {
    try (RedisLock redisLock = this.lockFactory.get(key)) {
        if (!redisLock.tryLock(5, TimeUnit.SECONDS)) {
            return "获取锁失败：" + key;
        }
    }
    return "获取锁成功：" + key;
}
```
3.  注解方式-自动释放锁
```
// code：用于区分业务，key：用于从参数中获取KEY
@MethodLock(code = "account", key = "#key")
@GetMapping("/{key}")
public String lock3(@PathVariable String key) {
    // 如果获取锁失败，会抛出RuntimeException
    return "获取锁成功，key：" + key;
}
```

#### 注意事项
1. 分布式锁支持可重入，只有同一线程才能支持重入，重入时，如果是手动控制， 请注意子方法需要调用unlock关闭，否则锁无法退出， 重入原理请参考java.util.concurrent.locks.Lock

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request
