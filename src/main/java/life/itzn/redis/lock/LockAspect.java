package life.itzn.redis.lock;

import lombok.RequiredArgsConstructor;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.DefaultParameterNameDiscoverer;
import org.springframework.core.ParameterNameDiscoverer;
import org.springframework.expression.EvaluationContext;
import org.springframework.expression.ExpressionParser;
import org.springframework.expression.spel.standard.SpelExpressionParser;
import org.springframework.expression.spel.support.StandardEvaluationContext;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;

/**
 * Lock锁切面
 * @author zhimou.qu
 * @since 2021/5/31
 */
@Aspect
@Component
@RequiredArgsConstructor
public class LockAspect {

    private final LockFactory lockFactory;

    private final ExpressionParser parser = new SpelExpressionParser();

    private final ParameterNameDiscoverer discoverer = new DefaultParameterNameDiscoverer();

    @Around("@annotation(methodLock)")
    public Object around(ProceedingJoinPoint joinPoint, MethodLock methodLock) throws Throwable {
        String lockKey = this.getLockKey(joinPoint, methodLock);
        try (RedisLock redisLock = this.lockFactory.get(lockKey)) {
            if (!redisLock.tryLock(methodLock.time(), methodLock.unit())) {
                throw new RuntimeException("获取分布式锁失败，key：" + lockKey);
            }
            return joinPoint.proceed();
        }
    }

    private String getLockKey(ProceedingJoinPoint joinPoint, MethodLock methodLock) {
        Method method = ((MethodSignature) joinPoint.getSignature()).getMethod();
        String[] parameterNames = discoverer.getParameterNames(method);
        if (null == parameterNames) {
            throw new IllegalArgumentException("参数不允许为空");
        }
        Object[] args = joinPoint.getArgs();
        EvaluationContext context = new StandardEvaluationContext();
        for (int i = 0; i < parameterNames.length; i++) {
            context.setVariable(parameterNames[i], args[i]);
        }
        if (methodLock.code().isEmpty()) {
            return parser.parseExpression(methodLock.key()).getValue(context, String.class);
        }
        return methodLock.code() + ":" + parser.parseExpression(methodLock.key()).getValue(context, String.class);
    }

}
