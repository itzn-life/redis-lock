package life.itzn.redis.lock;

import org.springframework.data.redis.connection.RedisConnectionFactory;
import org.springframework.data.redis.core.StringRedisTemplate;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.TimeUnit;

/**
 * 锁工厂类
 * @author zhimou.qu
 * @since 2021/5/31
 */
public class LockFactory {

    private final String prefix;

    private final long time;

    private final TimeUnit unit;

    private final StringRedisTemplate redis;

    private final Map<String, RedisLock> locks = new ConcurrentHashMap<>();

    /**
     * 获取默认时间锁
     * @param key 锁名
     * @return 锁
     */
    public RedisLock get(String key) {
        return this.get(key, time, unit);
    }

    /**
     * 获取指定时间锁
     * @param key 锁名
     * @param time 时间
     * @param unit 单位
     * @return 锁
     */
    public RedisLock get(String key, long time, TimeUnit unit) {
        return this.locks.computeIfAbsent(key, k -> new RedisLock(prefix + ":" + k, time, unit, redis));
    }

    /**
     * 清除过期锁
     */
    private void clearExpired() {
        new Thread(() -> {
            try {
                for (;;) {
                    long now = System.currentTimeMillis();
                    this.locks.entrySet().removeIf(e -> now > e.getValue().getMaxLockTime());
                    TimeUnit.MINUTES.sleep(1);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }).start();
    }

    public LockFactory(String prefix, long time, TimeUnit unit, RedisConnectionFactory redisConnectionFactory) {
        this.prefix = prefix;
        this.time = time;
        this.unit = unit;
        this.redis = new StringRedisTemplate(redisConnectionFactory);
        this.clearExpired();
    }

}
