package life.itzn.redis.lock;

import lombok.Data;
import lombok.RequiredArgsConstructor;
import lombok.SneakyThrows;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.script.DefaultRedisScript;
import org.springframework.data.redis.core.script.RedisScript;

import java.io.Closeable;
import java.util.Collections;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Condition;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * Redis分布式锁
 * @author zhimou.qu
 * @since 2021/5/31
 */
@Data
@RequiredArgsConstructor
public class RedisLock implements Lock, Closeable {

    /** 锁名称 */
    private final String key;

    /** 超时时间 */
    private final long time;

    /** 超时单位 */
    private final TimeUnit unit;

    /** 锁拥有者 */
    private volatile boolean owner;

    /** 上锁时间 */
    private volatile long lockAt;

    /** Redis */
    private final StringRedisTemplate redis;

    /** 锁ID */
    private final String id = UUID.randomUUID().toString();

    /** 本地锁 */
    private final ReentrantLock localLock = new ReentrantLock();

    /** 加锁脚本 */
    private final RedisScript<Long> lockScript = this.lockScript();

    /** 解锁脚本 */
    private final RedisScript<Long> unlockScript = this.unlockScript();

    @Override
    public void lock() {
        this.localLock.lock();
        for (;;) {
            try {
                while (!this.obtainLock()) {
                    TimeUnit.MILLISECONDS.sleep(100);
                }
                break;
            } catch (InterruptedException e) {
                // 无需中断
            }
        }
    }

    @Override
    @SneakyThrows
    public void lockInterruptibly() {
        this.localLock.lockInterruptibly();
        while (!this.obtainLock()) {
            TimeUnit.MILLISECONDS.sleep(100);
        }
    }

    @Override
    public boolean tryLock() {
        return this.localLock.tryLock() && this.obtainLock();
    }

    @Override
    @SneakyThrows
    public boolean tryLock(long time, TimeUnit unit) {
        long expire = System.currentTimeMillis() + unit.toMillis(time);
        if (!this.localLock.tryLock(time, unit)) {
            return false;
        }
        while (System.currentTimeMillis() <= expire) {
            if (this.obtainLock()) {
                return true;
            }
            TimeUnit.MILLISECONDS.sleep(100);
        }
        return false;
    }

    /**
     * 尝试获取锁
     * @return boolean
     */
    private boolean obtainLock() {
        Long execute = this.redis.execute(this.lockScript, Collections.singletonList(this.key), this.id, String.valueOf(this.unit.toMillis(this.time)));
        if (null == execute) {
            throw new RuntimeException("获取锁失败，key：" + this.key);
        }
        if (execute == 0) {
            return false;
        }
        if (execute == 1) {
            return this.owner = (this.lockAt = System.currentTimeMillis()) > 0;
        }
        if (execute == 2) {
            return this.owner;
        }
        throw new RuntimeException("获取锁错误，出现非法值：" + execute);
    }

    @Override
    public void unlock() {
        if (!this.localLock.isHeldByCurrentThread()) {
            return;
        }
        if (!this.owner || this.localLock.getHoldCount() > 1) {
            this.localLock.unlock();
            return;
        }
        try {
            Long execute = this.redis.execute(this.unlockScript, Collections.singletonList(this.key), this.id);
            if (null == execute) {
                throw new RuntimeException("释放锁失败，key：" + this.key);
            }
            if (execute == 1) {
                return;
            }
            if (execute == 2) {
                throw new RuntimeException("释放锁冲突，key：" + this.key);
            }
            if (execute == 3) {
                throw new RuntimeException("释放锁失效，key：" + this.key);
            }
            if (execute == 0) {
                throw new RuntimeException("释放锁失败，key：" + this.key);
            }
            throw new RuntimeException("释放锁错误，出现非法值：" + execute);
        } finally {
            this.owner = false;
            this.localLock.unlock();
        }
    }

    @Override
    public Condition newCondition() {
        throw new UnsupportedOperationException();
    }

    @Override
    public void close() {
        this.unlock();
    }

    /**
     * 获取最大锁时间
     * @return 时间戳
     */
    public long getMaxLockTime() {
        return this.lockAt + this.unit.toMillis(this.time);
    }

    /**
     * 加锁脚本[0:加锁失败,1:加锁成功,2:锁已存在]
     * @return long
     */
    private RedisScript<Long> lockScript() {
        String script = "local id = redis.call('GET', KEYS[1])\n" +
                        "if id == ARGV[1] then\n" +
                        "return 2\n" +
                        "elseif not id then\n" +
                        "redis.call('SET', KEYS[1], ARGV[1], 'PX', ARGV[2])\n" +
                        "return 1\n" +
                        "end\n" +
                        "return 0";
        return new DefaultRedisScript<>(script, Long.class);
    }

    /**
     * 解锁脚本[0:解锁失败,1:解锁成功,2:解锁冲突,3:锁不存在]
     * @return long
     */
    private RedisScript<Long> unlockScript() {
        String script = "local id = redis.call('GET', KEYS[1])\n" +
                        "if id == ARGV[1] then\n" +
                        "return redis.call('DEL', KEYS[1])\n" +
                        "elseif not id then\n" +
                        "return 3\n" +
                        "end\n" +
                        "return 2";
        return new DefaultRedisScript<>(script, Long.class);
    }

}
