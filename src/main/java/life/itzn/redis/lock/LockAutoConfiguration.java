package life.itzn.redis.lock;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.redis.connection.RedisConnectionFactory;

import java.util.concurrent.TimeUnit;

/**
 * 分布式锁自动配置
 * @author zhimou.qu
 * @since 2021/5/31
 */
@Configuration
public class LockAutoConfiguration {

    @Bean
    public LockFactory lockFactory(@Value("${lock.prefix:lock}") String prefix,
                                   @Value("${lock.time:60}") long time,
                                   @Value("${lock.unit:SECONDS}") TimeUnit unit,
                                   RedisConnectionFactory redisConnectionFactory) {
        return new LockFactory(prefix, time, unit, redisConnectionFactory);
    }

    @Bean
    public LockAspect lockAspect(LockFactory lockFactory) {
        return new LockAspect(lockFactory);
    }

}
