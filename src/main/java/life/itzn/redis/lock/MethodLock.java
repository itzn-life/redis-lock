package life.itzn.redis.lock;

import java.lang.annotation.*;
import java.util.concurrent.TimeUnit;

/**
 * 方法锁
 * @author zhimou.qu
 * @since 2021/5/31
 */
@Documented
@Target(ElementType.METHOD)
@Retention(RetentionPolicy.RUNTIME)
public @interface MethodLock {

    /**
     * 锁名编码
     * @return 字符
     */
    String code() default "";

    /**
     * 锁值SpEL
     * @return 字符
     */
    String key();

    /**
     * 锁超时时间
     * @return 时间
     */
    long time() default 60;

    /**
     * 锁超时单位
     * @return 单位
     */
    TimeUnit unit() default TimeUnit.SECONDS;

}
